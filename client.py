# import socket
# import pickle
# # Constants
# SERVER_IP = "127.0.0.1"
# SERVER_PORT = 12345
# BUFFER_SIZE = 1024


# # Client
# def client():
#     client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#     for seq_num in range(10):
#         packet = {"ack": False, "seq_num": seq_num, "data": f"Packet {seq_num}"}
#         data = pickle.dumps(packet)
#         client_socket.sendto(data, (SERVER_IP, SERVER_PORT))

#         while True:
#             response, server_address = client_socket.recvfrom(BUFFER_SIZE)
#             ack_packet = pickle.loads(response)
#             if ack_packet["ack"] and ack_packet["seq_num"] == seq_num:
#                 print(f"Received ACK for Packet {seq_num}")
#                 break
# client()


import socket

PORT = 5104
ADDRESS = "127.0.0.1"
MAX_LINE_WIDTH = 500
MAX_FILE_SIZE = 10000

# Define a function to send data over a socket connection
def mysend(sock, content):
    try:
        sock.sendall(content.encode())
    except socket.error as e:
        print(f"Error: Cannot send data - {e}")
        return False
    return True

# Read the input file
with open("input.txt", "r") as file:
    content = file.read()

# Create a socket
socket_desc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to the server
server_address = (ADDRESS, PORT)
print(f"Connecting to {ADDRESS} on port {PORT} ...")
try:
    socket_desc.connect(server_address)
except socket.error as e:
    print(f"Error: Cannot connect to the server - {e}")
    socket_desc.close()
    exit(1)

print("Connection established.")

# Communicate
print(f"Sending Message:\n{content}")
sent_bytes = mysend(socket_desc, content)
if not sent_bytes:
    socket_desc.close()
    exit(1)

# Close the socket
socket_desc.close()
