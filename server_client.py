# import socket
# import pickle

# # Constants
# SERVER_IP = "127.0.0.1"
# SERVER_PORT = 12345
# BUFFER_SIZE = 1024

# # Server
# def server():
#     server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     server_socket.bind((SERVER_IP, SERVER_PORT))

#     while True:
#         data, client_address = server_socket.recvfrom(BUFFER_SIZE)
#         packet = pickle.loads(data)

#         # Simulate packet loss with a 30% probability
#         if random.random() < 0.3:
#             continue

#         if packet["ack"]:
#             print(f"Received ACK from {client_address}")
#         else:
#             print(f"Received data: {packet['data']} from {client_address}")

#             ack_packet = {"ack": True, "seq_num": packet["seq_num"]}
#             ack_data = pickle.dumps(ack_packet)
#             server_socket.sendto(ack_data, client_address)

# # # Client
# # def client():
# #     client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# #     for seq_num in range(10):
# #         packet = {"ack": False, "seq_num": seq_num, "data": f"Packet {seq_num}"}
# #         data = pickle.dumps(packet)
# #         client_socket.sendto(data, (SERVER_IP, SERVER_PORT))

# #         while True:
# #             response, server_address = client_socket.recvfrom(BUFFER_SIZE)
# #             ack_packet = pickle.loads(response)
# #             if ack_packet["ack"] and ack_packet["seq_num"] == seq_num:
# #                 print(f"Received ACK for Packet {seq_num}")
# #                 break

# if __name__ == "__main__":
#     import random

#     server()
#     # In a real-world scenario, you would typically run the server in a separate process or on a different machine.

#     # client()


import socket
import os

PORT = 5104
BUFSIZE = 10000
OUTFILENAME = "result_server_python.txt"

# Invert the case of every character in a string
def invert_case(s):
    inverted = [char.swapcase() if char.isalpha() else char for char in s]
    return ''.join(inverted)

# Create a socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Allow reusing the same address and port quickly
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind the socket to the address and port
server_socket.bind(("", PORT))

# Listen for incoming connections
server_socket.listen(10)
print(f"Listening for connections on port {PORT} ...")

while True:
    # Accept a connection
    client_socket, client_address = server_socket.accept()
    print(f"Accepted connection from {client_address}")

    # Receive data from the client
    msg_buffer = client_socket.recv(BUFSIZE)
    if not msg_buffer:
        break

    # Invert the case of the received message
    inverted_msg = invert_case(msg_buffer.decode())

    # Write the inverted message to a file
    with open(OUTFILENAME, "w") as result_file:
        result_file.write(inverted_msg)
    print(f"Result written to {OUTFILENAME}")

    # Close the client socket
    client_socket.close()

# Close the server socket
server_socket.close()
